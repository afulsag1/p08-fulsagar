//
//  ViewController.swift
//  MyApp1
//
//  Created by Abhijit Fulsagar on 5/10/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

import UIKit
import  PaperOnboarding

class ViewController: UIViewController,PaperOnboardingDataSource,PaperOnboardingDelegate{

    @IBOutlet weak var onboardingview: OnboardingView!
    
  
    @IBOutlet weak var getstarted: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        onboardingview.dataSource=self
        onboardingview.delegate=self
    }

    func onboardingItemsCount() -> Int {
        return 7
    }
    func onboardingItemAtIndex(_ index: Int) -> OnboardingItemInfo {
        
        let backgroundcolor1=UIColor(red: 217/255, green: 72/255, blue: 89/255, alpha: 1)
        let backgroundcolor2=UIColor(red: 106/255, green: 166/255, blue: 211/255, alpha: 1)
        let backgroundcolor3=UIColor(red: 168/255, green: 72/255, blue: 89/255, alpha: 1)
        let backgroundcolor4=UIColor(red: 40/255, green: 50/255, blue: 108/255, alpha: 1)
        let backgroundcolor5=UIColor(red: 215/255, green: 75/255, blue: 63/255, alpha: 1)
        let backgroundcolor6=UIColor(red: 132/255, green: 155/255, blue: 22/255, alpha: 1)
        let backgroundcolor7=UIColor(red: 21/255, green: 55/255, blue: 99/255, alpha: 1)
        
        let titleFont=UIFont(name: "AvenirNext-Bold", size: 24)!
        let descriptionFont=UIFont(name: "AvenirNext-Regular", size: 18)!
        
    return [(#imageLiteral(resourceName: "game2048"),"2048 Game","It is a sliding block puzzle game",#imageLiteral(resourceName: "dot"),backgroundcolor1,UIColor.white,UIColor.white,titleFont,descriptionFont),
            
            (#imageLiteral(resourceName: "doodlejump"),"DoodleJump","In Doodle Jump, the aim is to guide a four-legged animal called 'The Doodler' up a never-ending series of platforms without falling.", #imageLiteral(resourceName: "dot"),backgroundcolor2,UIColor.white,UIColor.white,titleFont,descriptionFont),
            
            (#imageLiteral(resourceName: "subway"),"Subway Surfer","Subway Surfers is an endless runner mobile game.As the hooligans run, they grab gold coins out of the air while simultaneously dodging collisions with railway cars and other objects, and can also jump on top of the trains to evade capture.", #imageLiteral(resourceName: "dot"),backgroundcolor3,UIColor.white,UIColor.white,titleFont,descriptionFont),
            
            (#imageLiteral(resourceName: "piano"),"Piano Tiles","Piano Tiles 2 is a music arcade game. The game's aim is to tap the black tiles without making any mistakes.", #imageLiteral(resourceName: "dot"),backgroundcolor4,UIColor.white,UIColor.white,titleFont,descriptionFont),
            
            (#imageLiteral(resourceName: "color"),"Color Switch","Bounce up and fly through the matching hues! In Color Switch, you must have quick reflexes to score. Touch the change orb, and look at your new shade. Then, wait until the perfect time to glide through the next spinning circle!", #imageLiteral(resourceName: "dot"),backgroundcolor5,UIColor.white,UIColor.white,titleFont,descriptionFont),
            
            (#imageLiteral(resourceName: "sound1"),"Sound Memeory","It is memory testing game.", #imageLiteral(resourceName: "dot"),backgroundcolor6,UIColor.white,UIColor.white,titleFont,descriptionFont),
            
            (#imageLiteral(resourceName: "visualimage"),"Visual Recognition","This is final project. As the name suggest, it recognizes the images and display a general information about it.", #imageLiteral(resourceName: "dot"),backgroundcolor7,UIColor.white,UIColor.white,titleFont,descriptionFont)][index]
       
    }
    func onboardingConfigurationItem(_ item: OnboardingContentViewItem, index: Int) {
        
    }
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        if index<=5  {
            if (self.getstarted.alpha==1)
            {
                UIView.animate(withDuration: 0.2, animations:
                    {
                        self.getstarted.alpha=0
                })
            }
            
            
        }
    }
    func onboardingDidTransitonToIndex(_ index: Int) {
        if index==6
        {
            if (self.getstarted.alpha==0)
            {
                UIView.animate(withDuration: 0.4, animations:
                    {
                       // print(index)
                        self.getstarted.alpha=1
                })
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

