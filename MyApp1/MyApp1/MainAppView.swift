//
//  MainAppView.swift
//  MyApp1
//
//  Created by Abhijit Fulsagar on 5/10/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

import Foundation
import UIKit
import  PaperOnboarding
import VisualRecognitionV3
import AlamofireImage

class MainAppView: UIViewController{
    
    @IBOutlet weak var image: UIImageView!
    let key=""
    let version="2017-05-10"
   
   
    @IBAction func getimage(_ sender: Any) {
        let number=Int(arc4random_uniform(1000))
        let url=URL(string: "https://unsplash.it/200/300?image=\(number)")!
        image.af_setImage(withURL: url)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

