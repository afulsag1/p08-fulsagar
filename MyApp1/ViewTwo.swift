//
//  ViewTwo.swift
//  MyApp1
//
//  Created by Abhijit Fulsagar on 5/11/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

import Foundation
import VisualRecognitionV3
import AlamofireImage
import PaperOnboarding

 class ViewTwo:UIViewController {
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var imagebutton: UIButton!
    let apikey=""
    let version="2017-05-10"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    @IBAction func getimage(_ sender: Any) {
        self.imagebutton.isEnabled=false
        
       let number=Int(arc4random_uniform(1000))
        let url=URL(string: "https://unsplash.it/400/700?image=\(number)")!
        imageview.af_setImage(withURL: url)
        let visualrecognition=VisualRecognition(apiKey: apikey, version: version)
        let fail={(error:Error)in
            print(error)
            DispatchQueue.main.async {
                self.label.text="Image could not be processed"
                self.imagebutton.isEnabled=true
                            }
            
        
        }
        let url1=URL(string: "https://unsplash.it/50/100?image=\(number)")!
        visualrecognition.classify(image: url1.absoluteString,failure:fail)
        {classifiedImages in
            	if let classimage=classifiedImages.images.first
                {
                    print(classimage.classifiers)
                    //code to get actual image recognition
                    if let temp=classimage.classifiers.first?.classes.first?.classification{
                       DispatchQueue.main.async {
                            self.label.text="Hello"
                            print(temp)
                        self.imagebutton.isEnabled=true
                        }
                        
                    }
                }
                else{
                    DispatchQueue.main.async {
                         self.label.text="Image could not be determined"
                        self.imagebutton.isEnabled=true
                    }
                   
                }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
